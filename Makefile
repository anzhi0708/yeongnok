all:
	python3 setup.py sdist bdist_wheel

commit:
	black yeongnok/*.py
	black yeongnok/tools/*.py
	git commit -a
	git push

format:
	black yeongnok/*.py
	black yeongnok/tools/*.py
	mypy yeongnok/*.py
	mypy yeongnok/tools/*.py

clean:
	rm -rf dist build yeongnok.egg-info yeongnok/__pycache__ yeongnok/tools/__pycache__
